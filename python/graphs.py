# graphs.py for Graph class to store particular graph settings

# import reload
from importlib import reload


# class Graph to contain graph properties
class Graph(list):
    """Class Graph to contain graphical properties.

    Inherits from:
        list
    """

    def __init__(self):
        """Initialize a graph instance.

        Arguments:
            None

        Returns:
            None
        """

        # define boundaries and bins
        self.bounds = []
        self.bins = []

        # define ticks and labels
        self.ticks = []
        self.labels = []
        self.font = 12

        # define second axis ticks and labels
        self.ticksii = []
        self.labelsii = []

        # define mixing ratios for colors
        self.ratios = []

        # define graph boundaries
        self.left = 0
        self.right = 0
        self.top = 0

        # define histogram width, middle, and height
        self.width = 0
        self.middles = []
        self.heights = []

        # define curve
        self.curve = []

        # make axis labels
        self.title = ''
        self.dependent = ''
        self.independent = ''

        return