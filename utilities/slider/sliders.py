# sliders.py for combining pngs into a pdf slideshow

# import reload
from importlib import reload

# import system tools
import os
import json
import shutil

# import datetime
from datetime import datetime, timedelta
from time import time

# import numpy and math
import numpy
import math

# pdf maker
from PIL import Image
from PyPDF2 import PdfFileReader, PdfFileWriter


# create Slider class
class Slider(list):
    """Class Slider to create slides.

    Inherits from:
        list
    """

    def __init__(self, directory):
        """Initialize a slider instance.

        Arguments:
            folder: str, folder name of pngs
        """

        # set current time
        self.now = time()

        # set directory
        self.directory = directory

        # establish directory structure
        self._establish()

        return

    def __repr__(self):
        """Create string for on screen representation.

        Arguments:
            None

        Returns:
            str
        """

        # create representation
        representation = ' < Slider instance at: {} >'.format(self.directory)

        return representation

    def _establish(self):
        """Establish directory structure for output files.

        Arguments:
            None

        Returns:
            None
        """

        # make list of destinations
        destinations = [self.directory]

        # make directories if not already made
        for destination in destinations:

            # try to
            try:

                # access the contenst
                _ = os.listdir(destination)

            # unless the directory does not exit
            except FileNotFoundError:

                # in which case, make it
                os.mkdir(destination)

        return None

    def _see(self, directory):
        """See all the paths in a directory.

        Arguments:
            directory: str, directory path

        Returns:
            list of str, the file paths.
        """

        # make paths
        paths = ['{}/{}'.format(directory, path) for path in os.listdir(directory)]

        return paths

    def _stamp(self, message):
        """Start timing a block of code, and print results with a message.

        Arguments:
            message: str

        Returns:
            None
        """

        # get final time
        final = time()

        # calculate duration and reset time
        duration = round(final - self.now, 5)
        self.now = final

        # print duration
        print('took {} seconds.'.format(duration))

        # begin new block
        print(message)

        return None

    def arrange(self, bypass=False):
        """Arrange slides in order.

        Arguments:
            bypass: boolean, bypass choices?

        Returns:
            None
        """

        # get all original slides
        paths = self._see(self.directory)
        paths.sort()

        # split off numbers
        originals = [path.split('/')[-1].split(':')[-1] for path in paths]

        # set slides and detours to the full lists by default
        slides = [name for name in originals]
        detours = [path for path in paths]

        # if not bypassing
        if not bypass:

            # begin slides and detours
            slides = []
            detours = []

            # while X is not chosen
            while True:

                # print list of slides
                print('\nslides:')
                [print('{}) {}'.format(str(index).zfill(2), name)) for index, name in enumerate(slides)]

                # print list of originals
                print('\noriginals:')
                [print('{}) {}'.format(str(index).zfill(2), name)) for index, name in enumerate(originals)]

                # get input
                choice = input('next slide? ')

                # check for break condition
                if choice in ('X', 'XXX', 'x', 'xxx'):

                    # break
                    break

                # try to
                try:

                    # convert to int
                    choice = int(choice or 0)

                    # make choice
                    slide = originals[choice]
                    detour = paths[choice]

                    # add original and path to slides and detours
                    slides.append(slide)
                    detours.append(detour)

                    # remove from lists
                    originals = [name for name in originals if name != slide]
                    paths = [path for path in paths if path != detour]

                # unless index error
                except (TypeError, IndexError):

                    # in which case, skip
                    pass

        # rename files
        print('renaming files...')
        for index, (slide, detour) in enumerate(zip(slides, detours)):

            # construct destination
            destination = '{}/{}:{}'.format(self.directory, index + 100, slide)

            # rename file
            os.rename(detour, destination)

        return None

    def bring(self, inbound=None):
        """Bring a destop image into the slide images.

        Arguments:
            None

        Returns:
            None
        """

        # set default inbound directory
        inbound = inbound or '../../Desktop'

        # print desktop contents
        contents = [path for path in self._see(inbound) if '.png' in path]
        contents.sort()
        [print('{}) {}'.format(index, name.split('/')[-1])) for index, name in enumerate(contents)]

        # default choice to first
        index = 0

        # go through choices
        choice = 0
        while choice < 99:

            # get choice, defaulting to index
            choice = int(input('>>?') or index)
            index += 1

            # copy to desktop
            path = contents[choice]
            name = path.split('/')[-1]
            desktop = '{}/{}'.format(self.directory, name)
            shutil.copy(path, desktop)

        return None

    def fling(self, outbound=None):
        """Fling a copy of the slide to the desktop for manipulations.

        Arguments:
            None

        Returns:
            None
        """

        # set default outbound directory
        outbound = outbound or '../../Desktop'

        # print directory contents
        contents = self._see(self.directory)
        contents.sort()
        [print('{}) {}'.format(index, name.split('/')[-1])) for index, name in enumerate(contents)]

        # get choice
        choice = int(input('>>?'))

        # copy to desktop
        path = contents[choice]
        name = path.split('/')[-1].split(':')[-1]
        desktop = '{}/{}'.format(outbound, name)
        shutil.copy(path, desktop)

        return None

    def glue(self, destination, *paths):
        """Glue too slides together horizontally.

        Arguments:
            destination: str, name of combined slide
            *paths: unpacked list of image files to glue

        Returns:
            None
        """

        # get all images
        images = [numpy.array(Image.open('{}/{}'.format(self.directory, path))) for path in paths]

        # set defaul as first image
        combination = images[0]

        # if there are two images
        if len(images) == 2:

            # combine
            combination = numpy.concatenate([images[0], images[1]], axis=1)

        # otherwise, if there are four images
        if len(images) == 4:

            # make two strips
            strip = numpy.concatenate([images[0], images[1]], axis=1)
            stripii = numpy.concatenate([images[2], images[3]], axis=1)
            combination = numpy.concatenate([strip, stripii], axis=0)

        # resave
        combination = Image.fromarray(combination)
        combination.save('{}/{}'.format(self.directory, destination))

        return None

    def grow(self, path, destination, factor=2):
        """Glue too slides together horizontally.

        Arguments:
            path: str, file name
            destination: str, file name
            factor: int, growth factor

        Returns:
            None
        """

        # get image
        image = Image.open('{}/{}'.format(self.directory, path))

        # resize
        expansion = image.resize((math.floor(image.width * factor), math.floor(image.height * factor)), Image.ANTIALIAS)

        # save
        expansion.save('{}/{}'.format(self.directory, destination))

        return None

    def sidle(self):
        """Add a secondary image to the right of a primary image.

        Arguments (on input):
            primary: png file path, with greenscreen background
            left: int, upper left corner horizontal
            top: int, upper left corner vertical
            right: int, lower right horizontal
            bottom: int, lower right vertical
            secondary: png file path
            upper: int, upper vertical alignment on secondary
            lower: int, lower vertical alignment on secondary
            destination: str, file path

        Returns:
            None
        """

        # print directory contents
        contents = self._see(self.directory)
        contents.sort()
        [print('{}) {}'.format(index, name.split('/')[-1])) for index, name in enumerate(contents)]

        # input primary image info
        primary = contents[int(input('\nprimary image? '))]
        left = int(input('left corner? '))
        top = int(input('top corner? '))
        right = int(input('right corner? '))
        bottom = int(input('bottom corner? '))

        # input secondary image info
        secondary = contents[int(input('\nsecondary image? '))]
        upper = int(input('upper alignment? '))
        lower = int(input('lower alignment? '))

        # get destination file name
        destination = input('\nfile name? ')

        # open the background image and the graph
        image = Image.open(secondary)
        graph = Image.open(primary)

        # determine half width
        half = math.floor(graph.width / 2)

        # begin new canvas, same size as original graph
        canvas = numpy.ones((graph.height, graph.width, 4)) * 255
        canvas = canvas.astype('uint8')

        # check image height versus graph height
        ratio = graph.height / image.height

        # if the width is now too big
        if image.width * ratio > half:

            # adjust based on width
            ratio = half / image.width

        # resize image according to ratio
        image = image.resize((math.floor(image.width * ratio), math.floor(image.height * ratio)), Image.ANTIALIAS)

        # calculate margin
        margin = math.floor((graph.height - image.height) / 2)

        # insert onto canvas
        canvas[margin:image.height + margin, half:half + image.width, :] = numpy.array(image)

        # calculate new alignments
        upper = math.floor(upper * ratio)
        lower = math.floor(lower * ratio)

        # calculate ceiling and floor of original graph based on upper, lower distance
        bracket = image.height - (upper + lower)
        bracketii = graph.height - (top + bottom)
        racket = bracket / bracketii

        # take box from graph
        graph = numpy.array(graph)
        insert = graph[:, left:-right, :]
        insert = Image.fromarray(insert)

        # resize graph and insert
        insert = insert.resize((image.width, math.floor(insert.height * racket)), Image.ANTIALIAS)

        # insert at proper point
        ceiling = math.floor(top * racket)
        offset = (margin + upper) - ceiling
        block = canvas[offset:offset + insert.height, :insert.width, :]
        matrix = numpy.array(insert)[:block.shape[0], :block.shape[1], :]
        canvas[offset:offset + insert.height, :insert.width, :] = matrix

        # save graph
        Image.fromarray(canvas).save('{}/{}'.format(self.directory, destination))

        return None

    def slip(self, transparency=0.5, green=[245, 245, 245]):
        """Slap a background under a graph at a particlar location.

        Arguments:
            transparency: transparency of background
            green: tuple of int, greenscreen color

        Arguments (on input):
            background: png file path
            foreground: png file path, with greenscreen background
            left: int, upper left corner horizontal
            top: int, upper left corner vertical
            right: int, lower right horizontal
            bottom: int, lower right vertical
            destination: str, file path

        Returns:
            None
        """

        # print directory contents
        contents = self._see(self.directory)
        contents.sort()
        [print('{}) {}'.format(index, name.split('/')[-1])) for index, name in enumerate(contents)]

        # input background image info
        background = contents[int(input('\nslip which background image? '))]

        # input foreground image info
        foreground = contents[int(input('\nunder which foreground image? '))]
        left = int(input('left corner? '))
        top = int(input('top corner? '))
        right = int(input('right corner? '))
        bottom = int(input('bottom corner? '))

        # get destination file name
        destination = input('\nnew file name? ')

        # open the background image and the graph
        image = Image.open(background)
        graph = numpy.array(Image.open(foreground))

        # determine insert size
        height = graph.shape[0] - (top + bottom)
        width = graph.shape[1] - (left + right)

        # resize the image
        image = image.resize((width, height), Image.ANTIALIAS)
        array = numpy.array(image)

        # create mask
        hues = array[:, :, :3]

        print(f'hues: {hues.shape}')
        print(hues)

        summation = hues.sum(axis=2)
        mask = summation < sum(green) * 0.9

        print(f'summation: {summation.shape}')
        print(summation)
        print(f'mask: {mask.shape}')
        print(mask)
        print(mask[5:-5, 5:-5])

        # create film
        film = numpy.ones(mask.shape)
        film[mask] = transparency

        # apply transparency
        array[:, :, 3] = array[:, :, 3] * film

        # get the insert
        insert = graph[top:-bottom, left:-right, :]
        superimposition = insert.copy()

        # for each row
        for index, row in enumerate(insert):

            # and each column
            for indexii, pixel in enumerate(row):

                # check for green screen
                if all([color == channel for color, channel in zip(pixel[:3], green)]):

                    # replace pixel
                    superimposition[index][indexii] = array[index][indexii]

        # add back insert to graph
        graph[top:-bottom, left:-right, :] = superimposition

        # save graph
        Image.fromarray(graph).save('{}/{}'.format(self.directory, destination))

        return None

    def stitch(self, name, disposal=True, order=None, archive='../bibliotek/archive'):
        """Stitch all pdfs together into a compositie.

        Arguments:
            name: str, name for file
            disposal=True: remove intermediate files?
            order=None: list of str
            archive=None: str, list of archive folder to duplicate into

        Returns:
            None
        """

        # status
        self._stamp('stitching...')

        # append the date to the file
        date = datetime.fromtimestamp(self.now).strftime('%Ym%m%d')
        name = '{}_{}'.format(date, name)

        # load up pdfs
        source = self.directory
        pages = os.listdir(source)
        pages = [page for page in pages if '.png' in page and ':' in page]
        paths = ['{}/{}'.format(source, page) for page in pages]

        # convert pngs to temporary pdf files
        ghosts = [path.replace('.png', '.pdf') for path in paths]

        # open all images and convert to arrays
        images = [Image.open(path).convert('RGBA') for path in paths]
        arrays = [numpy.array(image) for image in images]
        shapes = [array.shape for array in arrays]
        height = max([shape[0] for shape in shapes])
        width = max([shape[1] for shape in shapes])

        # pad each array
        pads = []
        for array in arrays:

            # get the array shape
            shape = array.shape

            # adjust height to maximum
            pad = numpy.ones((height, shape[1], shape[2])) * 255
            pad = numpy.concatenate([array, pad], axis=0)
            pad = pad[:height]

            # adjust width to maximum
            half = int(width / 2)
            left = numpy.ones((height, half, 4)) * 255
            right = numpy.ones((height, half, 4)) * 255
            pad = numpy.concatenate([left, pad, right], axis=1)
            middle = int(pad.shape[1] / 2)
            pad = pad[:, middle - half: middle + width - half]

            # convert type
            pad = pad.astype('uint8')
            pads.append(pad)

        # save all pads to temporary pads if not already there
        [Image.fromarray(pad).convert('RGB').save(ghost) for pad, ghost in zip(pads, ghosts) if ghost.split('/')[-1] not in os.listdir(source)]

        # sort according to order
        ghosts.sort()
        if order:

            # go through each phrase backwards
            for index, phrase in enumerate(order):

                # sort each tail according to next phrase
                tail = ghosts[index:]
                tail.sort(key=lambda path: fuzz.ratio(path, phrase), reverse=True)
                ghosts = ghosts[:index] + tail

        # initialize writer
        writer = PdfFileWriter()

        # go through ghosts
        for ghost in ghosts:

            # print the ghost
            print(ghost)

            # read in ghost
            reader = PdfFileReader(ghost)
            for page in range(reader.getNumPages()):

                # add to writer
                writer.addPage(reader.getPage(page))

        # Write out the merged PDF
        destination = '{}/{}'.format(source, name)
        with open(destination, 'wb') as pointer:

            # write file
            writer.write(pointer)

        # if archiving
        if archive:

            # Write out the merged PDF
            destination = '{}/{}'.format(archive, name)
            with open(destination, 'wb') as pointer:

                # write file
                writer.write(pointer)

        # if disposal is true
        if disposal:

            # remove ghost files
            for ghost in ghosts:

                # remove
                os.remove(ghost)

        return None